Workflow Access Extension

Project Information

This module is a collection of helper modules extending Workflow's access capabilities and granularity.
It currently includes 3 extension modules:
- Workflow user reference access
- Workflow role reference access
- Workflow transitions access

These modules are particularly useful when working on advanced workflows, roles and rights/permissions structures for projects such as collaborative platforms or intranets. Concretely speaking, for those having worked with Open Atrium and workflow, this module was successfully tested and brings a useful addition to fulfill advanced requirements.

The Workflow user reference access and Workflow role reference access modules work in a very similar way, to provide dynamic resolution of node access rights/permissions for a particular state in a defined workflow. By changing values of user or role reference fields in a given node, different users or roles can be granted/revoked access to this node, according to a specific workflow.

Workflow user reference access
This module extends Workflow by allowing CCK user reference fields to grant referenced users specific permissions/rights on the node, according to Workflow's defined logic.
The workflow state form is extended to show all user reference fields with appropriate controls to grant or revoke specific access rights on the node for that particular state, to the referenced user(s) (value of the field).

Workflow user reference access comes as an addition to the Workflow Fields module and actually adds another level of granularity for referenced users to not only have access to fields, but also node level, including view, edit, delete permissions on nodes.

USAGE
When the module is enabled, the "Edit workflow state" page is augmented with a list of user reference fields that correspond to the content types using the workflow. You can specify whether each user reference field should enable or not a specific permission for referenced user value(s), when the node's workflow is in that particular state.

Use case example:
Author uid1 has the right to change in the node nid1 the value of the user reference field to uid2.
User uid2 is granted the rights/permissions on the node defined in the workflow admin page in a particular state.

Workflow role reference access

This module extends Workflow by allowing the CCK Role Reference field module to grant referenced roles specific permissions/rights on the node, according to workflow's defined logic.
The workflow state form is extended to show all role reference fields with appropriate controls to grant or revoke specific access rights on the node for that particular state, to the user(s) within the referenced role(s) (value of the field).

Workflow role reference access comes as an addition to the workflow fields module and actually adds another level of granularity for users within referenced roles to not only have access to fields, but also node level, including view, edit, delete permissions on nodes.

USAGE
When the module is enabled, the "Edit workflow state" page is augmented with a list of role reference fields that correspond to the content types using the workflow. You can specify whether each role reference field should enable or not a specific permission for referenced role value(s), when the node's workflow is in that particular state.

Use case example:
Author uid1 has the right to change in the node nid1 the value of the role reference field to role1.
User uid2 belonging to role1 is granted the rights/permissions on the node defined in the workflow admin page in a particular state.

Concretely speaking, this module allows opening a node in a specific role when the node's workflow is in a particular state.

Workflow transitions access

This module extends Workflow and transitions, in particular, by adding another layer of validation for a user to be able to change a node's status, to initiate a transition.
It requires at least the Workflow user reference access module, but also integrates well with Workflow role reference access.
The Workflow module allows definition of which roles can perform which transitions (From status A to status B).
With this extension, users have to validate another condition, based on user reference (user is being referenced) or role reference (user's role is being referenced) in order to gain access to the original workflow transition rights.

USAGE
When the module is enabled, the "Edit workflow state" page is augmented with a list of transitions that correspond to the defined workflow. For each transition, a list of user and role reference fields are displayed with appropriate controls to grant or revoke specific access rights on the node for that particular transition.

Use case example:
This module is used to prevent certain users within a particular role from being able to get the permissions granted by the workflow module as defined on the transitions configuration pages.

Drupal Integration:
These modules are fully integrated with other contributed modules and Drupal's API through implementation of modules' specific hooks: Workflow, CCK, Node access system, mostly.
They were successfully tested with Views and perfectly integrate with it.

Integration with features for a full consistent export of workflow with all related modules and objects is planned to be added. Currently under work and should part of next version releases.