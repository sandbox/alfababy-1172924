<?php
/**
 * @file Features support.
 * Workflow Transitions Access Features integration.
 */


/**
 * Implementation of hook_features_export_options().
 */
function workflow_transitions_access_features_export_options() {
	$workflow = workflow_get_all();
	$options = array();
	foreach($workflow as $key => $name){
		$options[$name] = $name;
	}
	
	return $options;
}

/**
 * Implementation of hook_features_export().
 */
function workflow_transitions_access_features_export($data, &$export, $module_name = '') {
	// The filter_default_formats() hook integration is provided by the
	// features module so we need to add it as a dependency.
	$export['dependencies']['features'] = 'features';
	$export['dependencies']['workflow'] = 'workflow';
	$export['dependencies']['workflow_userreference_access'] = 'workflow_userreference_access';
	$export['dependencies']['workflow_transitions_access'] = 'workflow_transitions_access';
	
	$states = array();
	foreach($data as $wid){
		$states = _workflow_transitions_access_get_states($wid) + $states;
	}
	
	foreach($data as $w_name){
		$export['features']['workflow_trans_access'][$w_name] = $w_name;
	}
	
  return array();
}

/**
 * Implementation of hook_features_export_render().
 */
function workflow_transitions_access_features_export_render($module, $data) {
	$code = array();
	$code[] = '  $workflow_trans_access = array();';
	$code[] = '';

	$states = array();
	foreach($data as $w_name){
		$states = _workflow_transitions_access_get_states($w_name) + $states;
	}
	
	$workflow_trans_access = _workflow_get_trans_access($states);

	foreach($states as $sid => $state){
		$workflow_trans = $workflow_trans_access['workflow_trans_access'][$sid];
		
		$workflow_name = features_var_export($state->workflow_name);
		$workflow_state_name = features_var_export($state->state_name);
		$workflow_trans_export = features_var_export($workflow_trans, '  ');
		$code[] = "  // Exported workflow transitions access: {$state->workflow_name} - {$state->state_name}";
		$code[] = "  \$workflow_trans_access[{$workflow_name}][{$workflow_state_name}] = {$workflow_trans_export};";
		$code[] = "";
	}
	
	$code[] = '  return $workflow_trans_access;';
  $code = implode("\n", $code);
  return array('features_default_workflow_trans_access' => $code);
}

/**
 * Implementation of hook_features_export_revert().
 */
function workflow_transitions_access_features_revert($module) {
  workflow_transitions_access_features_rebuild($module);
}

/**
 * Implementation of hook_features_rebuild().
 */
function workflow_transitions_access_features_rebuild($module) {
	if ($defaults = features_get_default('workflow_trans_access', $module)) {
		foreach($defaults as $w_name => $states){
			$wid = db_result(db_query("SELECT wid FROM {workflows} WHERE name = '%s'", $w_name));
			
			if(empty($wid)){
				//TODO: Add new workflow first
			}
			
			if(!empty($wid)){
				_workflow_transitions_access_add_new_access($wid, $w_name, $states, TRUE);
			}
		}
	}
}

function _workflow_transitions_access_get_states($w_name){
	$all_workflow = workflow_get_all();
	$wid = array_search($w_name, $all_workflow);
	
	//$workflow_name = workflow_get_name($wid);
	$states = workflow_get_states($wid);
	$states_name = array();
	if(!is_array($states)){
		return $states_name;
	}
	
	foreach($states as $sid => $state){
		//$states_name[$sid] = $workflow_name .': '.$state;
		$states_name[$sid] = (object)array('workflow_name'=>$w_name, 'state_name'=>$state);
	}
	return $states_name;
}

function _workflow_get_trans_access($states){
	$sids = implode(',', array_keys($states));
	$result = db_query("SELECT * FROM {workflow_transitions_access} WHERE sid in (%s)", empty($sids) ? 0 : $sids);
	
	while($state = db_fetch_object($result)){
		$workflow_trans_access[$state->sid][$state->type][$state->fieldname] = array('transition'=>$state->transition, 'transition_type'=>$state->transition_type);
	}
	
	return array('workflow_trans_access'=>$workflow_trans_access);
}

function _workflow_transitions_access_add_new_access($wid, $w_name, $states, $reset = FALSE){
	$current_states_name = workflow_get_states($wid);
	$current_states_sid = array_keys($current_states_name);

	if($reset){
		//Delete old states
		db_query('DELETE FROM {workflow_transitions_access} WHERE sid IN (%s)', implode(',', $current_states_sid));
	}else{
		$sids = array();
		foreach($states as $state_name => $state){
			$sids[] = array_search($state_name, $current_states_name);
		}
		$sids = array_filter($sids);
		db_query('DELETE FROM {workflow_transitions_access} WHERE sid IN (%s)', count($sids) > 0 ? implode(',', $sids) : 0);
	}	
	
	foreach($states as $state_name => $state){
		if($state == NULL) CONTINUE;		
		$sid = array_search($state_name, $current_states_name);
		foreach($state as $ct_name => $fields){
			foreach($fields as $field_name => $access){
				db_query("INSERT INTO {workflow_transitions_access} (sid, type, fieldname, transition, transition_type) VALUES (%d, '%s', '%s', %d, '%s')", $sid, $ct_name, $field_name, $access['transition'], $access['transition_type']);
			}
		}
	}
}
