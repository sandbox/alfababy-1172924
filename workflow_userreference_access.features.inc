<?php
/**
 * @file Features support.
 * Workflow Userreference Access Features integration.
 */


/**
 * Implementation of hook_features_export_options().
 */
function workflow_userreference_access_features_export_options() {
	$workflow = workflow_get_all();
	$options = array();
	foreach($workflow as $key => $name){
		$options[$name] = $name;
	}
	
	return $options;
}

/**
 * Implementation of hook_features_export().
 */
function workflow_userreference_access_features_export($data, &$export, $module_name = '') {
	// The filter_default_formats() hook integration is provided by the
	// features module so we need to add it as a dependency.
	$export['dependencies']['features'] = 'features';
	$export['dependencies']['workflow'] = 'workflow';
	$export['dependencies']['workflow_userreference_access'] = 'workflow_userreference_access';
	
	$states = array();
	foreach($data as $wid){
		$states = _workflow_userreference_access_get_states($wid) + $states;
	}
	
	foreach($data as $w_name){
		$export['features']['workflow_uref_access'][$w_name] = $w_name;
	}
	
  return array();
}

/**
 * Implementation of hook_features_export_render().
 */
function workflow_userreference_access_features_export_render($module, $data) {
	$code = array();
	$code[] = '  $workflow_uref_access = array();';
	$code[] = '';

	$states = array();
	foreach($data as $w_name){
		$states = _workflow_userreference_access_get_states($w_name) + $states;
	}
	
	$workflow_uref_access = _workflow_get_userref_access($states);

	foreach($states as $sid => $state){
		$workflow_uref = $workflow_uref_access['workflow_uref_access'][$sid];
		
		$workflow_name = features_var_export($state->workflow_name);
		$workflow_state_name = features_var_export($state->state_name);
		$workflow_uref_export = features_var_export($workflow_uref, '  ');
		$code[] = "  // Exported workflow userreference access: {$state->workflow_name} - {$state->state_name}";
		$code[] = "  \$workflow_uref_access[{$workflow_name}][{$workflow_state_name}] = {$workflow_uref_export};";
		$code[] = "";
	}
	
	$code[] = '  return $workflow_uref_access;';
  $code = implode("\n", $code);
  return array('features_default_workflow_uref_access' => $code);
}

/**
 * Implementation of hook_features_export_revert().
 */
function workflow_userreference_access_features_revert($module) {
  workflow_userreference_access_features_rebuild($module);
}

/**
 * Implementation of hook_features_rebuild().
 */
function workflow_userreference_access_features_rebuild($module) {
	if ($defaults = features_get_default('workflow_uref_access', $module)) {
		foreach($defaults as $w_name => $states){
			$wid = db_result(db_query("SELECT wid FROM {workflows} WHERE name = '%s'", $w_name));
			
			if(empty($wid)){
				//TODO: Add new workflow first
			}
			
			if(!empty($wid)){
				_workflow_userreference_access_add_new_access($wid, $w_name, $states);
			}
		}
	}
}

function _workflow_userreference_access_get_states($w_name){
	$all_workflow = workflow_get_all();
	$wid = array_search($w_name, $all_workflow);
	
	//$workflow_name = workflow_get_name($wid);
	$states = workflow_get_states($wid);
	$states_name = array();
	if(!is_array($states)){
		return $states_name;
	}
	
	foreach($states as $sid => $state){
		//$states_name[$sid] = $workflow_name .': '.$state;
		$states_name[$sid] = (object)array('workflow_name'=>$w_name, 'state_name'=>$state);
	}
	return $states_name;
}

function _workflow_get_userref_access($states){
	$sids = implode(',', array_keys($states));
	$result = db_query("SELECT * FROM {workflow_userreference_access} WHERE sid in (%s)", empty($sids) ? 0 : $sids);
	
	while($state = db_fetch_object($result)){
		$workflow_uref_access[$state->sid][$state->type][$state->fieldname] = array('grant_view'=>$state->grant_view, 'grant_update'=>$state->grant_update, 'grant_delete'=>$state->grant_delete);
	}
	
	return array('workflow_uref_access'=>$workflow_uref_access);
}

function _workflow_userreference_access_add_new_access($wid, $w_name, $states){
//	$default_states_name = array_keys($states);
	$current_states_name = workflow_get_states($wid);
	$current_states_sid = array_keys($current_states_name);

	if($reset){
		//Delete old states
		db_query('DELETE FROM {workflow_userreference_access} WHERE sid IN (%s)', implode(',', $current_states_sid));
	}else{
		$sids = array();
		foreach($states as $state_name => $state){
			$sids[] = array_search($state_name, $current_states_name);
		}
		$sids = array_filter($sids);
		db_query('DELETE FROM {workflow_userreference_access} WHERE sid IN (%s)', count($sids) > 0 ? implode(',', $sids) : 0);
	}	
	
	foreach($states as $state_name => $state){
		if($state == NULL) CONTINUE;		
		$sid = array_search($state_name, $current_states_name);		
		foreach($state as $ct_name => $fields){			
			foreach($fields as $field_name => $access){
				db_query("INSERT INTO {workflow_userreference_access} (sid, type, fieldname, grant_view, grant_update, grant_delete) VALUES (%d, '%s', '%s', %d, %d, %d)", $sid, $ct_name, $field_name, $access['grant_view'], $access['grant_update'], $access['grant_delete']);
			}
		}
	}
}
